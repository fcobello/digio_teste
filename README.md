![build](https://img.shields.io/bitbucket/pipelines/fcobello/digio_teste/master)

# digio-test

Aplicação construida para avaliação Digio

## Pré requisitos
Java 11 (https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)

Maven (https://maven.apache.org/download.cgi)

## Como executar
Após download do projeto executar o comando

`mvn spring-boot:run`

## Como testar
### Inserir lançamento

`curl -X POST 'http://localhost:8080/lancamentos-contabeis' -H 'Content-Type: application/json' -d '{"valor":100.25, "data":"20200101", "contaContabil":1111001}'`

### Buscar lançamento por id (trocar o id por um válido)

`curl -X GET 'http://localhost:8080/lancamentos-contabeis/22723e58-d948-4e5a-b061-633be171806c'`

### Buscar lançamento por conta contabil (trocar por uma conta contabil válida)

`curl -X GET 'http://localhost:8080/lancamentos-contabeis/?contaContabil=1111001'`

### Buscar estatisticas dos lançamentos

`curl -X GET 'http://localhost:8080/lancamentos-contabeis/stats'`

### Busca estatisticas dos lançamentos por conta contabil (trocar por uma conta contabil válida)
`curl -X GET 'http://localhost:8080/lancamentos-contabeis/stats/?contaContabil=1111001'`

## Como acessar o banco de dados
Para esse projeto foi utilizado o banco H2 em memoria, o banco é recriado a cada start da aplicação

É possivel acessar o banco através de uma WebConsole (http://localhost:8080/h2-console), os dados de usuario, senha e url podem ser obtidos no arquivo application.properties