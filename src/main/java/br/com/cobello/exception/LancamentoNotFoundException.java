package br.com.cobello.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exceção para tratar eventos de lançamentos não encontrados
 * @author Felipe
 *
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class LancamentoNotFoundException extends RuntimeException{

	private static final long serialVersionUID = 1161839313944002352L;

	public LancamentoNotFoundException(String message) 
	{
		super(message);
	}

}
