package br.com.cobello.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Entidade que representa os lançamentos
 * @author Felipe
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Lancamentos 
{
	@Id
	private String id;
	private long contaContabil;
	private Date data;
	private BigDecimal valor;
}
