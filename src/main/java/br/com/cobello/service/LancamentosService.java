package br.com.cobello.service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.cobello.entity.Lancamentos;
import br.com.cobello.exception.LancamentoNotFoundException;
import br.com.cobello.pojo.LancamentosRequest;
import br.com.cobello.pojo.LancamentosStatsResponse;
import br.com.cobello.repository.LancamentosRepository;

/**
 * Classe de serviço para tratamento das operações referentes aos lançamentos como registro, consulta, etc
 * @author Felipe
 *
 */
@Service
public class LancamentosService {

	@Autowired
	LancamentosRepository repository;

	/**
	 * Registra um lançamento
	 * @param request
	 * @return
	 */
	public Lancamentos registrar(LancamentosRequest request) {
		
		final Lancamentos db = new Lancamentos();

		db.setContaContabil(request.getContaContabil());
		db.setData(request.getData());
		db.setId(UUID.randomUUID().toString());
		db.setValor(request.getValor());
		repository.save(db);
		
		return db;
	}
	
	/**
	 * Consulta um lançamento
	 * @param id identificação do lançamento
	 * @return
	 * @throws LancamentoNotFoundException
	 */
	public Lancamentos buscar(String id) throws LancamentoNotFoundException 
	{
		
		Optional<Lancamentos> db = repository.findById(id);
		
		if (!db.isPresent())
		{
			throw new LancamentoNotFoundException("Lançamento não encontrado");
		}
		
		return db.get();
	}
	
	/**
	 * Consulta um lançamento
	 * @param contaContabil conta contabil
	 * @return
	 * @throws LancamentoNotFoundException
	 */
	public List<Lancamentos> buscarContaContabil(long contaContabil) throws LancamentoNotFoundException 
	{
		List<Lancamentos> lstDb = repository.findByContaContabil(contaContabil);
		
		if (lstDb == null || lstDb.isEmpty())
		{
			throw new LancamentoNotFoundException("Lançamento não encontrado");
		}
		
		return lstDb;
	}
	
	/**
	 * Consulta estatisticas de lançamentos. Se a conta contabil estiver presente será usada como filtro
	 * 
	 * @param contaContabil conta contabil
	 * @return
	 */
	public LancamentosStatsResponse buscarStats(final Long contaContabil) 
	{
		final LancamentosStatsResponse result;
		
		if (contaContabil != null)
		{
			result = repository.findStatsByContaContabil(contaContabil);
		}
		else
		{
			result = repository.findStats();
		}
		
		if (result.getQtde() == 0)
		{
			throw new LancamentoNotFoundException("Sem Lançamentos");
		}
		
		return result;
	}
}
