package br.com.cobello.api;

import javax.validation.Valid;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.cobello.pojo.LancamentosRequest;
import br.com.cobello.pojo.LancamentosResponse;
import br.com.cobello.service.LancamentosService;
import lombok.extern.slf4j.Slf4j;

/**
 * API de Lançamentos Contabeis
 * 
 * <p>Trata os endpoints
 * <p><b>/lancamentos-contabeis</b><br>
 * <b>/lancamentos-contabeis/{id}</b><br>
 * <b>/lancamentos-contabeis/?contacontabil={contacontabil}</b>
 * @author Felipe
 *
 */
@RestController
@Slf4j
@Validated
public class LancamentosAPI extends BaseAPI
{
	@Autowired
	LancamentosService service;
	
	/**
	 * Metodo responsavel pelo tratamento das requisições do tipo {@link PostMapping} para inserir um novo lançamento
	 * @param request
	 * @return
	 */
	@PostMapping("/lancamentos-contabeis")
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public Object lancamentos(@Valid @RequestBody LancamentosRequest request)
	{
		log.info("Registro de Lançamento [{}]", request);
		return (new LancamentosResponse(service.registrar(request).getId()));
	}
	
	/**
	 * Metodo responsavel pelo tratamento das requisições do tipo {@link GetMapping} para buscar um lançamento pelo seu id
	 * @param id
	 * @return
	 */
	@GetMapping("/lancamentos-contabeis/{id}")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Object lancamentosId(@PathVariable @Size(min=36, max=36) String id)
	{
		log.info("Consulta de Lançamento por ID [{}]", id);
		
		return service.buscar(id);
	}
	
	/**
	 * Metodo responsavel pelo tratamento das requisições do tipo {@link GetMapping} para buscar todos os lançamentos de uma conta contabil
	 * @param contaContabil
	 * @return
	 */
	@GetMapping("/lancamentos-contabeis")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Object lancamentosContaContabil(@RequestParam long contaContabil)
	{
		log.info("Consulta de Lançamento por Conta Contabil [{}]", contaContabil);
		
		return service.buscarContaContabil(contaContabil);
	}
}
