package br.com.cobello.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.cobello.service.LancamentosService;
import lombok.extern.slf4j.Slf4j;

/**
 * API de Estatisticas dos Lançamentos Contabeis
 * 
 * <p>Trata os endpoints
 * <p><b>/lancamentos-contabeis/stats</b><br>
 * <b>/lancamentos-contabeis/stats/?contacontabil={contacontabil}</b>
 * @author Felipe
 *
 */
@RestController
@Slf4j
public class LancamentosStatsAPI extends BaseAPI
{
	@Autowired
	LancamentosService service;
	
	/**
	 * Metodo responsavel pelo tratamento das requisições do tipo {@link GetMapping} para buscar estatisticas dos lançamentos de uma conta contabil
	 * @param contaContabil
	 * @return
	 */
	@GetMapping("/lancamentos-contabeis/stats")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Object lancamentosStatsContaContabil(@RequestParam(required = false) Long contaContabil)
	{
		log.info("Consulta de Estatisticas: Conta Contabil [{}]", contaContabil);
		
		return service.buscarStats(contaContabil);
	}
}
