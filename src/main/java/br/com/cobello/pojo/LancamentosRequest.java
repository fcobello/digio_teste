package br.com.cobello.pojo;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

/**
 * Pojo da Solicitação da Requisição de Lançamentos
 * @author Felipe
 *
 */
@Data
public class LancamentosRequest 
{
	@Min(1)
	@Max(99999999999l)
	private long contaContabil;
	@DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
	@JsonFormat(pattern = "yyyyMMdd")
	private Date data;
	@NotNull (message = "valor não pode ser nulo")
	private BigDecimal valor;
}
