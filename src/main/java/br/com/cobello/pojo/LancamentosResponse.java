package br.com.cobello.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Pojo da Resposta da requisição de Lançamentos
 * @author Felipe
 *
 */
@Data
@AllArgsConstructor
public class LancamentosResponse 
{
	private String id;
}
