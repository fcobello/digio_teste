package br.com.cobello.pojo;

import java.math.BigDecimal;

import lombok.Data;

/**
 * Pojo da Resposta da requisição de estatisticas de lançamentos
 * @author Felipe
 *
 */
@Data
public class LancamentosStatsResponse {
	private BigDecimal soma;
	private BigDecimal min;
	private BigDecimal max;
	private Double media;
	private Long qtde;

	public LancamentosStatsResponse(BigDecimal soma, BigDecimal min, BigDecimal max, Double media, Long qtde) {
		this.soma = soma == null ? new BigDecimal(0) : soma;
		this.min = min == null ? new BigDecimal(0) : min;
		this.max = max == null ? new BigDecimal(0) : max;
		this.media = media == null ? 0 : media;
		this.qtde = qtde == null ? 0 : qtde;
	}
}
