package br.com.cobello.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.cobello.entity.Lancamentos;
import br.com.cobello.pojo.LancamentosStatsResponse;

/**
 * Interface para as operações de consulta no repositorio
 * @author Felipe
 *
 */
@Repository
public interface LancamentosRepository extends CrudRepository<Lancamentos, String>{
	
	/**
	 * Consulta os lançamentos usando a conta contabil como filtro
	 * @param contaContabil
	 * @return
	 */
	List<Lancamentos> findByContaContabil(long contaContabil);
	
	/**
	 * Consulta as estatisticas (soma, minimo, maximo, media e quantidade) de todos os lançamentos
	 * @return
	 */
	@Query("SELECT new br.com.cobello.pojo.LancamentosStatsResponse(sum(l.valor), min(l.valor), max (l.valor), avg(l.valor), count(l.valor)) from Lancamentos as l")
	LancamentosStatsResponse findStats();
	
	/**
	 * Consulta as estatisticas (soma, minimo, maximo, media e quantidade) de todos os lançamentos usando a conta contabil como filtro
	 * @param contaContabil
	 * @return
	 */
	@Query("SELECT new br.com.cobello.pojo.LancamentosStatsResponse(sum(l.valor), min(l.valor), max (l.valor), avg(l.valor), count(l.valor)) from Lancamentos as l where l.contaContabil = ?1")
	LancamentosStatsResponse findStatsByContaContabil(long contaContabil);
}
