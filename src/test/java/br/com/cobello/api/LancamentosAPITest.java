package br.com.cobello.api;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

/**
 * Testes Unitarios das APIs de Lançamentos Contabeis e Estatisticas
 * 
 * @author Felipe
 *
 */
@TestMethodOrder(OrderAnnotation.class)
@SpringBootTest
@AutoConfigureMockMvc
public class LancamentosAPITest {

	@Autowired
	MockMvc mock;

	@Test
	@Order(1)
	public void testRegitro() throws Exception {
		mock.perform(post("/lancamentos-contabeis").contentType(MediaType.APPLICATION_JSON)
				.content("{\"valor\":432424, \"data\":\"20201301\", \"contaContabil\":12345678901}"))
				.andExpect(status().isCreated());
	}

	@Test
	@Order(2)
	public void testConsultar() throws Exception {
		mock.perform(get("/lancamentos-contabeis").queryParam("contaContabil", "12345678901"))
				.andExpect(status().isOk());
	}

	@Test
	@Order(3)
	public void testConsultar404() throws Exception {
		mock.perform(get("/lancamentos-contabeis/?contaContabil=1")).andExpect(status().isNotFound());
	}

	@Test
	@Order(4)
	public void testConsultarStatsContaContabil() throws Exception {
		mock.perform(get("/lancamentos-contabeis/stats").queryParam("contaContabil", "12345678901"))
				.andExpect(status().isOk());
	}

	@Test
	@Order(5)
	public void testConsultarStats() throws Exception {
		mock.perform(get("/lancamentos-contabeis/stats")).andExpect(status().isOk());
	}

	@Test
	@Order(6)
	public void testConsultarStats404() throws Exception {
		mock.perform(get("/lancamentos-contabeis/stats").queryParam("contaContabil", "1"))
				.andExpect(status().isNotFound());
	}
}
